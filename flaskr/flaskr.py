#!/usr/bin/python
# -*- coding: utf-8 -*-

################ Bibliotecas utilizadas ##########################
from flask import Flask, request, session, g, Markup, redirect, url_for, abort, \
	 render_template, flash, jsonify, escape
from flask_sqlalchemy import SQLAlchemy
#from flask.ext.bcrypt import Bcrypt
import bcrypt
from unicodedata import normalize
import random
import json
import socket
import time
import datetime
import netifaces as ni
import sys
#import secrets

###################################################################

app = Flask(__name__)
#bcrypt = Bcrypt(app)
app.config.from_object(__name__)

app.config['DEBUG'] = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///gameservice.sqlite3'

app.config['SECRET_KEY'] = "random string"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

####################################################################
####################################################################
##############################Banco de dados##########################

class Users(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))
	email = db.Column(db.String(50))
	password = db.Column(db.String(50))
	token = db.Column(db.String(50))
	saves = db.relationship('Saves', backref='users', lazy='dynamic')

class Saves(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.DateTime)
	itens = db.Column(db.String(50))
	places = db.relationship('Places', backref='saves', lazy='dynamic')
	id_users = db.Column(db.Integer, db.ForeignKey('users.id_'))

class Places(db.Model):
	code = db.Column(db.Integer, primary_key=True)
	id_ = db.Column(db.Integer)
	isUnlocked = db.Column(db.Boolean)
	isVisited = db.Column(db.Boolean)
	itens = db.Column(db.String(50))
	id_saves = db.Column(db.Integer, db.ForeignKey('saves.id_'))


####################################################################
##############################Login######################################

@app.route('/api/login',  methods=['POST'])
def login():
	if request.method == 'POST':
		email 	 = request.form['email']
		password = request.form['password']
		user = Users.query.filter_by(email=email).first()
		if user is None:
			response = 'erro'
		else:
			if user.email == email and user.password == password:
				user.token = genarateToken(user.id_)
				db.session.add(user)
				db.session.commit()
				save = structureSave(user.id_, user.token)

				response = jsonify(save)

			else:
				response = 'erro'

	else:
		response = '404'
	return response
##################################################################
###################strucutureSave#############################
def structureSave(user_id,token):
	saves = Saves.query.filter_by(id_users=user_id).all()

	if not saves:
		save_date = datetime.datetime.now()
		save_item = '[1,3,4]'
		new_save = Saves(datetime=save_date,itens=save_item,id_users=user_id)
		db.session.add(new_save)
		db.session.commit()
		id_ = new_save.id_
		new_place = Places(id_=1,isUnlocked = False, isVisited = True, itens='[1,5,6]', id_saves=id_)
		db.session.add(new_place)
		db.session.commit()
	save = Saves.query.filter_by(id_users=user_id).order_by("datetime desc").first()
	save_id = save.id_
	places = Places.query.filter_by(id_saves=save_id).all()
	lastSave = {}
	lastSave['token'] = token
	lastSave['datetime'] = save.datetime
	lastSave['itens'] = save.itens
	lastSave['places'] = []
	for place in places:
		lastSave['places'].append({'id': place.id_, 'isUnlocked': place.isUnlocked, 'isVisited': place.isVisited, 'itens': place.itens})
		# lastSave['places']['id'] = place.id_
		# lastSave['places']['isUnlocked'] = place.isUnlocked
		# lastSave['places']['isVisited'] = place.isVisited
		# lastSave['places']['itens'] = place.itens

	return lastSave

##################################################################
##################Index########################################

@app.route('/')
def index():
	return render_template('index.html')

#################################################################################
###############################Register##########################################


@app.route('/register', methods=['POST', 'GET'])
def register():

	if request.method == 'POST':
		name      = request.form['name']
		email     = request.form['email']
		password  = request.form['password']
		if not name or not email or not password:
			message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Os campos nao podem ser enviados vazios!</h4><p></p></div>")
			flash(message)
			return render_template('index.html')
		else:
			user = Users.query.filter_by(email=email).first()
			if user is None:
				user = Users(name=name,email=email,password=password)
				db.session.add(user)
				db.session.commit()
				message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Cadastrado com sucesso!</h4><p></p></div>")
			else:
				message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Usuário ja cadastrado!</h4><p></p></div>")
			
			flash(message)
			return render_template('index.html')
	else:
		return render_template('index.html')

###################################################################################################################
####################################Update#########################################################################
@app.route('/api/save/update', methods=['POST', 'GET', 'PUT'])
def update():
	if request.method == 'POST':
		save_update = request.form['save_update'];
		# jsonUpdate = request.get_json()
		#jsonUpdate = '{"token": "123456","date": "2018−02−28T20:29:16.331Z","inventory": [4 , 6 , 2 , 1 ],"places": [{"id" : 1 ,"isUnlocked" : true ,"isVisited" : true ,"item" : [1 ,4]} ,{"id" : 3 ,"isUnlocked" : true ,"isVisited" : false ,"items" : [3 , 5]}]}'
		save_update = json.loads(save_update)

		token = save_update['token']
		itens = save_update['itens']
		places = save_update['places']
		save_date = datetime.datetime.now()
		user = Users.query.filter_by(token=token).first()
		if user:
			# user.token = genarateToken(user.id_)
			new_save = Saves(datetime=save_date, itens=itens, id_users=user.id_)
			db.session.add(new_save)
			db.session.commit()
			id_ = new_save.id_
			for place in places:
				new_place = Places(id_=place['id'],isUnlocked = place['isUnlocked'], isVisited =  place['isVisited'],itens=place['itens'], id_saves=id_)
				db.session.add(new_place)
				db.session.commit()
			save = structureSave(user.id_, user.token)
			response = jsonify(save)
		else:
			response = 'erro'
	else:
		response = 'erro'
	return response
@app.route('/api/save/autentication', methods=['POST'])
def autenticate():
	if request.method == 'POST':
		token = request.form['token']
		datetime = request.form['datetime']
		user = Users.query.filter_by(token=token).first()
		if user is None:
			response = 'erro'
		else:
			user.token = genarateToken(user.id_)
			db.session.add(user)
			db.session.commit()
			save = structureSave(user.id_, user.token)
			response = jsonify(save)
	else:
		response = '404'
	return response

def genarateToken(user_id):
	hash = 'macacos me mordam'
	pw_hash = bcrypt.hashpw(hash.encode('utf8'), bcrypt.gensalt())
	candidate = str(pw_hash) + str(random.random() + user_id) + str(random.random()) + str(user_id)
	token = str(bcrypt.hashpw(candidate.encode('utf8'), bcrypt.gensalt()))
	#return hash
	return  normalize('NFKD', token).encode('ASCII', 'ignore').decode('ASCII')
###################################################################################################################
###################################################################################################################
###################################################################################################################
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
if __name__ == '__main__':
	#app.run(debug = True)
	db.create_all()
	db.session.commit()
	#cmd = "ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
	#p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
	#Recebendo o IP que está na placa no eth0
	#AdressIP, err = p.communicate()
	#AdressIP = socket.gethostbyname(socket.gethostname())
	#reload(sys)
	#sys.setdefaultencoding('utf8')
	ni.ifaddresses('eno1')
	ip = ni.ifaddresses('eno1')[ni.AF_INET][0]['addr']
	app.run(host=ip, port=5000, debug=True, threaded=True)
